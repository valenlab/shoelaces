#include <Python.h>
#include "api/BamMultiReader.h"
#include "api/BamAux.h"



#ifdef _WIN32
#include <tr1/unordered_map>
#elif defined __unix__
#include <tr1/unordered_map>
using namespace std::tr1;
#elif defined __APPLE__
#include <unordered_map>
using namespace std;
#endif

#include <exception>
using namespace BamTools;

static int getLength(BamAlignment read)
{

    if (read.CigarData.size() == 1)
        return read.Length;

    int length = 0;
    //std::cout << "cigar:";
    for (int i = 0; i < read.CigarData.size(); ++i)
    {
        CigarOp t = read.CigarData[i];
        length += t.Length;
        //std::cout << t.Type:'

    }

    return length;
}

static double getForwardReadPos(BamAlignment read, int offset)
{
    int32_t sum = 0;
    int32_t skip = 0;

    int32_t pos = read.Position ;
    for (int i = 0; i < read.CigarData.size(); ++i)
    {
        CigarOp t = read.CigarData[i];

        int32_t nextStep = t.Length;
        if (t.Type == 'N')
        {
            skip += nextStep;
        }
        else
        {
            if (sum + nextStep > -offset)
                break;
            sum += nextStep;
        }
    }
    pos += (-offset + skip);
    return pos;
}

static double getBackwardReadPos(BamAlignment read, int offset)
{
    int32_t sum = 0;
    int32_t skip = 0;



    int32_t pos = read.Position + getLength(read);
    //std::cout << "length: " << getLength(read) << std::endl;
    for (int i = read.CigarData.size()-1; i >=0; --i)
    {
        CigarOp t = read.CigarData[i];

        int32_t nextStep = t.Length;
        if (t.Type == 'N')
        {
            skip += nextStep;
        }
        else
        {
            if (sum + nextStep > -offset)
                break;
            sum += nextStep;
        }
    }
    pos -= (-offset + skip);
    return pos;
}

static double getRealReadPos(BamAlignment read, int offset)
{
    if (read.IsReverseStrand())
        return getBackwardReadPos(read, offset);
    else
        return getForwardReadPos(read, offset);
}


static PyObject* py_myFunction(PyObject* self, PyObject* args)
{
    const char *filename;
    const char *chromosome;
    PyObject * dict;

    if (!PyArg_ParseTuple(args, "sOs", &filename, &dict, &chromosome))
    {
        printf("Item is not a string\n");
        return NULL;
    }
    //printf("Item is a string\n");

    BamReader br;

    if(!br.Open(filename))
    {
        printf("Error opening file\n");
    }

    unordered_map<int, int> offsets;

    PyObject * keys = PyDict_Keys(dict);
    int nrKeys = PyList_GET_SIZE(keys);
    printf("%d keys", nrKeys);
    for (int i = 0 ; i < nrKeys; ++i)
    {
        int value = PyLong_AsLong(PyDict_GetItem(dict, PyList_GetItem(keys, i)));
        int key = PyLong_AsLong(PyList_GetItem(keys, i));
        std::pair<int, int> p(key,value);
        offsets.insert(p);
        printf("%d value in %d \n",value, key);
    }


    std::vector<RefData> chromosomes = br.GetReferenceData();
    //int chromosomeLength  = 0;
    PyObject * pyAll = PyDict_New();
    PyObject * pyChromosomeToKey = PyDict_New();
    for (int i = 0; i < chromosomes.size();++i)
    {
        PyObject * id = PyLong_FromLong(br.GetReferenceID(chromosomes[i].RefName));
        PyObject * name = PyUnicode_FromString(chromosomes[i].RefName.c_str());
        //PyDict_SetItem(pyAll, id, PyDict_New());

        PyDict_SetItem(pyAll, name, PyDict_New());
        PyDict_SetItem(pyChromosomeToKey, id, name);
    }



    int sum = 0;

    //std::cout << chromosome << std::endl;
    //int refID = br.GetReferenceID(chromosome);
    //BamRegion region(refID, 0, refID, -1);
    //br.SetRegion(region);


    BamAlignment ba;
    bool bContinue = true;
    while (bContinue)
    {
        try
        {
            bContinue = br.GetNextAlignment(ba);
        }
        catch(exception e)
        {
            bContinue = false;
        }

        continue;
        PyObject * pHistKey =  PyDict_GetItem(pyChromosomeToKey, PyLong_FromLong(ba.RefID));

        PyObject * pyResult = PyDict_GetItem(pyAll, pHistKey);
        int length = ba.Length;

        int offset = 0;
        offset = offsets[length];
        int position = getRealReadPos(ba, offset);

        PyObject * pKey = PyLong_FromLong(position);
        PyObject * pCurrent = PyDict_GetItem(pyResult, pKey);

        int current = 1;
        if (pCurrent)
            current = PyLong_AsLong(pCurrent) + 1;

        PyDict_SetItem(pyResult, pKey, PyLong_FromLong(current));

        sum++;

    }


    //printf("%d reads processed\n",sum);

    return pyAll;

}


static PyObject* py_createSplitWig(PyObject* self, PyObject* args)
{
    const char *filename;
    const char *chromosome;
    PyObject * dict;

    if (!PyArg_ParseTuple(args, "sOs", &filename, &dict, &chromosome))
    {
        printf("Item is not a string\n");
        return NULL;
    }
    //printf("Item is a string\n");

    BamReader br;

    if(!br.Open(filename))
    {
        printf("Error opening file\n");
    }

    unordered_map<int, int> offsets;

    PyObject * keys = PyDict_Keys(dict);
    int nrKeys = PyList_GET_SIZE(keys);
    //printf("%d keys", nrKeys);
    for (int i = 0 ; i < nrKeys; ++i)
    {
        int value = PyLong_AsLong(PyDict_GetItem(dict, PyList_GetItem(keys, i)));
        int key = PyLong_AsLong(PyList_GetItem(keys, i));
        std::pair<int, int> p(key,value);
        offsets.insert(p);
        //printf("%d value in %d \n",value, key);
    }


    std::vector<RefData> chromosomes = br.GetReferenceData();
    //int chromosomeLength  = 0;


    PyObject * pyChromosomeToKey = PyDict_New();
    for (int i = 0; i < chromosomes.size();++i)
    {
        PyObject * id = PyLong_FromLong(br.GetReferenceID(chromosomes[i].RefName));
        PyObject * name = PyUnicode_FromString(chromosomes[i].RefName.c_str());
        //PyDict_SetItem(pyAll, id, PyDict_New());

        //PyDict_SetItem(pyAll, name, PyDict_New());
        PyDict_SetItem(pyChromosomeToKey, id, name);
    }



    int sum = 0;

    //std::cout << chromosome << std::endl;
    //int refID = br.GetReferenceID(chromosome);
    //BamRegion region(refID, 0, refID, -1);
    //br.SetRegion(region);

    PyObject * pyForward = PyDict_New();
    PyObject * pyReverse = PyDict_New();

    BamAlignment ba;
    PyObject * pyAll;
    int multimappers = 0;
    bool bContinue = true;
    while (bContinue)
    {
        try
        {
            bContinue = br.GetNextAlignment(ba);
        }
        catch(exception e)
        {
            bContinue = false;

        }
        if (!bContinue)
            break;
        uint8_t nh= 0 ;
        bool b = ba.GetTag("NH", nh);
        if (b and int(nh) > 1){

            multimappers++;
            continue;
        }

        if (ba.IsReverseStrand())
        {
            pyAll = pyReverse;
        }
        else
        {
            pyAll = pyForward;
        }


        int length = ba.Length;
        PyObject * pCurrentLengthWig =PyDict_GetItem(pyAll, PyLong_FromLong(length));
        if (!pCurrentLengthWig)
        {
            PyDict_SetItem(pyAll, PyLong_FromLong(length), PyDict_New());
            pCurrentLengthWig =PyDict_GetItem(pyAll, PyLong_FromLong(length));
        }



        PyObject * pHistKey =  PyDict_GetItem(pyChromosomeToKey, PyLong_FromLong(ba.RefID));

        /////error
        //printf("%d value in %d \n",length, ba.RefID);
        if (ba.RefID < 0)
            continue;
        PyObject * pyResult = PyDict_GetItem(pCurrentLengthWig, pHistKey);

        if (!pyResult)
        {
            PyDict_SetItem(pCurrentLengthWig, pHistKey, PyDict_New());
            pyResult = PyDict_GetItem(pCurrentLengthWig, pHistKey);
        }



        int offset = 0;
        offset = offsets[length];
        int position = getRealReadPos(ba, offset);

        PyObject * pKey = PyLong_FromLong(position);
        PyObject * pCurrent = PyDict_GetItem(pyResult, pKey);

        int current = 1;
        if (pCurrent)
            current = PyLong_AsLong(pCurrent) + 1;

        PyDict_SetItem(pyResult, pKey, PyLong_FromLong(current));

        sum++;


    }
    std::cout << "Multimappers: "  << multimappers << std::endl;
    std::cout << "Total: "  << sum << std::endl;
    std::string fwd = "forward";
    std::string bwd = "reverse";
    std::string mm = "multimappers";

    PyObject * pyResult = PyDict_New();
    PyDict_SetItem(pyResult, PyUnicode_FromString(fwd.c_str()), pyForward);
    PyDict_SetItem(pyResult, PyUnicode_FromString(bwd.c_str()), pyReverse);
    PyDict_SetItem(pyResult, PyUnicode_FromString(mm.c_str()), PyLong_FromLong(multimappers));

    return pyResult;

}
/*
 * Bind Python function names to our C functions
 */
static PyMethodDef myModule_methods[] = {
  {"myFunction", py_myFunction, METH_VARARGS},
  {"createSplitWig", py_createSplitWig, METH_VARARGS},
  {NULL, NULL}

};

static struct PyModuleDef statsmodule = {
   PyModuleDef_HEAD_INIT,
   "statsmodule",   /* name of module */
   NULL, /* module documentation, may be NULL */
   -1,       /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
   myModule_methods
};

/*
 * Python calls this to let us initialize our module
 */
PyMODINIT_FUNC
PyInit_statsmodule(void)
{
    return PyModule_Create(&statsmodule);
}
