#version 150

uniform mat4 projection;
uniform float gap;
uniform float vgap;
uniform float hspace;

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

void main() {
    //vec4 pos = gl_in[0].gl_Position;
    //vec4 pos1 = vec4(pos.xy, 0, 1);
    //vec4 pos2 = vec4(pos.x + pos.z, pos.y, 0, 1);
    //vec4 pos3 = vec4(pos.x, pos.y + 30, 0, 1);
    //vec4 pos4 = vec4(pos.x + pos.z, pos.y+30, 0, 1);

    vec4 pos = gl_in[0].gl_Position;
    pos.x += (gap * pos.z);

    vec4 pos1 = vec4(pos.x - hspace, vgap, 0, 1);
    vec4 pos2 = vec4(pos.x + hspace, vgap , 0, 1);
    vec4 pos3 = vec4(pos.x - hspace, pos.y + vgap, 0, 1);
    vec4 pos4 = vec4(pos.x + hspace, pos.y+vgap, 0, 1);

    gl_Position = projection * pos1;
    EmitVertex();

    gl_Position = projection * pos2;
    EmitVertex();

    gl_Position = projection * pos3;
    EmitVertex();

    gl_Position = projection * pos4;
    EmitVertex();

    EndPrimitive();
}